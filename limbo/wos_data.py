def clean_scopus_ref(txtdata):
    """
    Copied from: https://github.com/cortext/cortext-methods/blob/36007111da80f538b3c54a9de2ffbf3e8494d631/pylibrary/fonctions.py#L4520
    """
    # print 'txtdata',txtdata
    clause = False
    try:
        m = re.search(r"\(([0-9]{4})\)", txtdata)

        year = m.groups()[0]
        try:
            if str(int(year)) == year:  # cool
                clause = True
            else:
                print(year, "n'est une année")
        except:
            try:
                print(year, "n'est une année")
            except:
                pass
    except:
        pass  # print '!!!\t can\'t decode',txtdata
    if clause:
        st = m.start()
        en = m.end()
        beforeyear = txtdata[:st].strip()
        afteryear = txtdata[en:].strip()
        # print len(beforeyear)
        # print len(afteryear)
        if (
            len(beforeyear) > 0
        ):  # one should find some string looking like this Kottke, F., Medical education for comprehensive medical care (1965) Arch. Phys. Med. Rehabilit., 46, pp. 349-353;
            beforev = beforeyear.split(".,")
            # print 'beforev',beforev
            citedreftitle = beforev[-1]
            if len(beforev) > 1:
                citedauthors = map(lambda x: x.replace(",", "").strip(), beforev[:-1])

            # 	for x in range((len(beforev)-1)/2):
            # 		citedauthors.append(' '.join(beforev[2*x:2*x+2]).strip())
            else:
                citedauthors = []
        else:
            citedauthors = []
        if len(afteryear) > 0:
            citedjournal = afteryear.split(",")[0].strip()
            if len(citedjournal) == 0:  # cases where a comma follows year
                try:
                    citedjournal = afteryear.split(",")[1].strip()
                except:
                    pass

        else:
            citedjournal = ""

        try:
            firstauthor = citedauthors[0]
        except:
            firstauthor = ""
        # print 'afteryear',afteryear,len(afteryear)
        citedref = firstauthor + "_" + str(year) + "_" + citedjournal
    else:
        citedref, citedauthors, citedjournal, year = False, False, False, False
    return citedref, citedauthors, citedjournal, year


def treat_references_in_notices(notices):
    """
    Copied from: https://github.com/cortext/cortext-methods/blob/13b836c63f6a1c1cc080c8ad67a6d9a79ba80219/parser_science/parser_science_se.py#L2595
    """
    for cle, notice in notices.iteritems():
        citedrefl, citedauthorsl, citedjournall, citedyearl = [], [], [], []
        if "Rawcitation" in notice:
            # print notice['CitedRef'][0]
            for ref in notice["Rawcitation"][0]:
                # print 'ref',ref
                (
                    citedref,
                    citedauthors,
                    citedjournal,
                    citedyear,
                ) = fonctions.clean_scopus_ref(ref)
                # print '---', citedref
                # print '---', citedauthors
                # print '---',citedjournal
                # print '---', citedyear
                if not citedref == False:
                    if len(citedref) > 0:
                        citedrefl.append([citedref])
                    if len(citedauthors) > 0:
                        citedauthorsl.append(citedauthors)
                    if len(citedjournal) > 0:
                        citedjournall.append([citedjournal])
                    if len(citedyear) > 0:
                        citedyearl.append([citedyear])

        if len(citedrefl) > 0:
            notice["Cited_Ref"] = citedrefl
            # print "notice['Cited_Ref']=[citedrefl]",notice['Cited_Ref']
        if len(citedjournall) > 0:

            notice["Cited_Journal"] = citedjournall

        if len(citedauthorsl) > 0:
            notice["Cited_Authors"] = citedauthorsl
        if len(citedyearl) > 0:
            notice["Cited_Years"] = citedyearl
        if "Adress" in notice:
            # print ("sqdnotice[col]",notice[col])
            liste = notice["Adress"][0]
            temp = []
            for l in liste:
                # print (l)
                temp.append([l])
            notice["Adress"] = temp
            # print 'sqdnotice[col]',notice[col]
    # print ('noticeadre',notice)
