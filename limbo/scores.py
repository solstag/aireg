import pandas


def herve_scores():
    df = pandas.read_csv("data/sel_on_overton_meta_docs_20210710.csv")
    return df[["pdf_document_id", "global_score"]]
