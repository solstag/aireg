# AI Regulation

Source code for the AI Regulation project.

## Installation

Clone the repository and `pip install -e aireg`

## Extra dependencies
Manual installation is required for some dependencies.

- graph-tool: Instructions at <https://graph-tool.skewed.de>

### For old data only
- arcpath: Copy <https://gitlab.com/cortext/cortext-methods/istex/-/blob/main/tarpath.py> somewhere in your PYTHONPATH

## Usage

Import the module and check the code for its submodules:
- "data": download and load the data
- "sashimi": set up and perform domain-topic modeling
- "reporting": essential interfaces, histograms and stats
- "exploration": tables and figures; plus heatmaps based on manual coding

```python
import aireg
```
