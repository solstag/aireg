from pathlib import Path
import logging as log
from PIL import Image

import colorcet
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from sashimi import GraphModels

from .sashimi import sashimi_reload
from .data import manual_filter


log.root.setLevel("INFO")

DPI = 300


def get_corpus(start_2k=False):
    corpus = sashimi_reload()
    corpus.load_domain_topic_model()
    corpus.data = prep_data(corpus)
    if start_2k:
        corpus.data = corpus.data[lambda x: x[corpus.col_time].ge(2000)]
    return corpus


def prep_data(corpus: GraphModels):
    data = corpus.data.copy()
    data = data.rename({"_time": "year"}, axis=1)
    data["year"] = data.year.astype("Int64")
    return data


def plot_for_all_domains(
    out_dir: Path, plot_function, corpus: GraphModels, domain_labels=None
):
    if domain_labels is None:
        domain_labels = [
            label for label in corpus.label_to_hblock.keys() if "D" in label
        ]
    for dlabel in domain_labels:
        if int(dlabel[1]) >= 3:
            log.info(f"plotting domain: {dlabel}")
            data = get_data_for_domains(corpus, dlabel)
            domain_out_dir = out_dir / dlabel
            domain_out_dir.mkdir(exist_ok=True)
            plot_function(domain_out_dir=domain_out_dir, data=data)


def get_data_for_domains(corpus: GraphModels, *domain_labels):
    return corpus.data[corpus.domain_labels_to_selection(domain_labels)]


##########
# Tables #
##########


def plot_tables_for_all_fields_for_all_domains(
    out_dir: Path, corpus: GraphModels, domain_labels=None
):
    plot_for_all_domains(
        out_dir,
        plot_tables_for_all_fields,
        corpus,
        domain_labels=domain_labels,
    )


def plot_tables_for_all_fields(out_dir: Path, data):
    for field in [
        "authors",
        "source__source_id",
        "source__title",
        "cites__people__person",
        "cites__scholarly__doi",
    ]:
        log.info(f"plotting table: {field}")
        plot_tables_for_field(out_dir, data, field)


def plot_tables_for_field(out_dir: Path, data: pd.DataFrame, field: str):
    data: pd.DataFrame = data[["year", field]].copy()
    data[field] = data[field].map(lambda x: set(x) if isinstance(x, list) else x)

    s_col = (
        data[field]
        .explode()
        .value_counts()
        .sort_index()
        .sort_values(kind="stable", ascending=False)
        .rename("abs")
        .to_frame()
    )
    s_col["rel"] = s_col["abs"].div(len(data))

    year_col = (
        data.explode(field)
        .groupby("year")[field]
        .value_counts()
        .rename("abs")
        .to_frame()
    )
    year_col["rel"] = year_col["abs"].div(data["year"].value_counts(), level=0)

    nat_col = (
        data.loc[data["year"].isna()][field]
        .explode()
        .value_counts()
        .sort_index()
        .sort_values(kind="stable", ascending=False)
        .rename("abs")
        .to_frame()
    )
    nat_col["rel"] = nat_col["abs"].div(data["year"].isna().sum())

    html_args = {
        "float_format": "{:.1%}".format,
        "bold_rows": False,
        "index_names": False,
    }
    doc = (
        f"""<!DOCTYPE html><html><head><title>{field}</title>"""
        + """<style>
        table {
          display: block; max-height: 20em; overflow-y: scroll;
          width: fit-content; max-width: 50%; border:0;
        }
        table, th, td {border-collapse: collapse; padding: .1em}
        </style>
        </head><body>"""
        + f"""<h1>{field}: totals</h1>"""
        + f"""{s_col.to_html(**html_args)}"""
        + f"""<h1>{field}: per year</h1>"""
        + "".join(
            f"""<h2>{y}</h2>{year_col.loc[y].to_html(**html_args)}"""
            for y in year_col.index.get_level_values("year").unique()
        )
        + f"""<h1>{field}: missing year</h1>"""
        + f"""{nat_col.to_html(**html_args)}"""
        + """</html></body>"""
    )
    (out_dir / f"frequency_of_{field}.html").write_text(doc)


###########
# Figures #
###########


def plot_all_figures_for_all_domains(out_dir: Path, corpus: GraphModels):
    plot_for_all_domains(
        out_dir=out_dir,
        plot_function=lambda domain_out_dir, data: displot(
            out_dir=domain_out_dir,
            data=data,
            x="year",
            hue="source__country",
            row="source__type",
        ),
        corpus=corpus,
        domain_labels=None,
    )
    plot_for_all_domains(
        out_dir=out_dir,
        plot_function=lambda domain_out_dir, data: displot(
            out_dir=domain_out_dir,
            data=data,
            x="year",
            hue="source__country",
        ),
        corpus=corpus,
        domain_labels=None,
    )
    plot_for_all_domains(
        out_dir=out_dir,
        plot_function=lambda domain_out_dir, data: displot(
            out_dir=domain_out_dir,
            data=data,
            x="year",
            hue="source__type",
        ),
        corpus=corpus,
        domain_labels=None,
    )
    plot_for_all_domains(
        out_dir=out_dir,
        plot_function=lambda domain_out_dir, data: displot(
            out_dir=domain_out_dir,
            data=data,
            x="year",
        ),
        corpus=corpus,
        domain_labels=None,
    )


def displot(
    out_dir: Path,
    data,
    x,
    y=None,
    hue=None,
    row=None,
    col=None,
    fname_prefix="",
    fname_suffix="",
):
    log.info(f"plotting for: {locals()}")
    for i in plt.get_fignums():
        plt.close(plt.figure(i))
    limit = 10
    over_is_time = x == "year"

    if hue:
        data = data.explode(hue)
        sel_by = data[hue].value_counts().index[:limit]
        data = data.loc[data[hue].isin(set(sel_by))]
    if not over_is_time:
        sel_over = data[x].value_counts().index[:limit]
        data = data.loc[data[x].isin(set(sel_over))]
    if y:
        sel_over_y = data[y].value_counts().index[:limit]
        data = data.loc[data[y].isin(set(sel_over_y))]

    single_kwargs = (
        dict(
            multiple="stack",
            shrink=0.9,
        )
        if y is None
        else {}
    )

    # sorter = data[field_over].value_counts().get
    if row == "source__type":
        row_order = [
            name
            for name in ["government", "think tank", "igo", "other"]
            if data[row].eq(name).any()
        ]
    norm_dims = [dim for dim in (x, y, row, col) if dim is not None]
    norm_counts = data.groupby(norm_dims).size()
    norm = data.agg(lambda x: 100 / norm_counts.loc[tuple(x[norm_dims])], axis=1)
    figs = []
    for subplot, weights in enumerate([None, norm]):
        fig = sns.displot(
            data=data,
            x=x,
            y=y,
            weights=weights,
            discrete=over_is_time,
            hue=hue,
            hue_order=sel_by if hue else None,
            aspect=16 / 9,
            kind="hist",
            palette=colorcet.glasbey_bw_minc_20[:limit],
            stat="count",
            row=row,
            row_order=row_order if row else None,
            col=col,
            **single_kwargs,
        )
        fig.tick_params(rotation=45)
        if weights is not None:
            fig.set_axis_labels(y_var="Proportion (%)")
        # if hue:
        #     ax.legend.set_title(hue)
        figs.append(fig)
    save_double_horizontal_fig(
        *figs,
        out_dir
        / (
            f"{fname_prefix}"
            f"document"
            f"_over_{x}{f'_X_{y}' if y else ''}"
            f"{f'_by_{hue}' if hue else ''}"
            f"{f'_row_{row}' if row else ''}"
            f"{f'_col_{col}' if col else ''}"
            f"{fname_suffix}.png"
        ),
    )


def relplot(out_dir, data, field_x, field_y=None, field_hue=None, fname_prefix=""):
    log.info(f"plot2{fname_prefix}")
    if field_y is None:
        data = data.dropna(subset=field_x).explode(field_hue)
        vc = data.groupby(field_x).value_counts(field_hue)
        data = vc.to_frame("count").reset_index()
        tnorm = data.groupby(field_x)["count"].sum()
        data["ncount"] = (data.set_index(field_x)["count"].div(tnorm)).array

    for field in [field_y] if field_y is not None else ["count", "ncount"]:
        sel_by = data[field].value_counts().index[:16]
        data = data.loc[data[field].isin(set(sel_by))]
        plt.close()
        plt.delaxes()  # noqa
        fig = sns.violinplot(
            data,
            x=field,
            y=field_x,
            order=sel_by,
            hue=field_hue,
            orient="v",
            scale="count",
        )
        fig.tick_params(rotation=45)
        fig.xaxis.set_label("Year")
        # fig.legend().set_title("Citation type")
        plt.savefig(
            out_dir
            / f"{fname_prefix}relation-{field}-over-{field_x}-with-{field_hue}.png",
            dpi=DPI,
        )


def domains_yblocks_matrix(corpus: GraphModels, domain_labels, yblock_labels):
    df = pd.DataFrame(index=domain_labels, columns=yblock_labels + ["any"], dtype=float)
    tlbs = [corpus.label_to_tlblock[label] for label in yblock_labels]
    ybtypes, ylevels, yblocks = zip(*tlbs)
    (ybtype,) = set(ybtypes)
    (ylevel,) = set(ylevels)
    for xlabel in df.index:
        xbtype, xlevel, xblock = corpus.label_to_tlblock[xlabel]
        s = corpus.get_xblock_yblocks_stat(
            "frac_presence", xbtype, xlevel, xblock, ybtype, ylevel, yblocks
        )
        s.index = s.index.map(
            lambda yblock: yblock
            if yblock == "any"
            else corpus.lblock_to_label[ylevel, yblock]
        )
        df.loc[xlabel] = s
    df = df.where(df.notna(), 0.0)
    return df


def domain_topic_heatmap(corpus: GraphModels):
    import matplotlib.pyplot as plt

    domains = manual_filter.get_domain_list(manual_filter.get_manual_data())
    topic_lists = manual_filter.get_topic_lists()
    dfs = []
    # figures for each group
    for key, topics in topic_lists.items():
        # data
        df = domains_yblocks_matrix(corpus, domains, topics)
        df = df.loc[reversed(df.index)].sort_values("any", ascending=False)
        # plot
        plt.close()
        plt.figure(figsize=(13, 10))
        plt.title(f"Domains and {key.capitalize()} topics")
        htm = sns.heatmap(df, annot=True, linewidths=0.2)
        htm.figure.savefig(f"heatmap domains and {key} topics.png")
        df.name = key
        dfs.append(df)
    # figures for all group agregates
    # data
    df = pd.DataFrame({df.name: df["any"] for df in dfs})
    df = df.sort_values(df.columns[0], ascending=False)
    # plot
    plt.close()
    plt.figure(figsize=(13, 10))
    plt.title("Domains and topic groups (any)")
    htm = sns.heatmap(df, annot=True, linewidths=0.2)
    htm.figure.savefig("heatmap domains and topic groups.png")


def save_double_horizontal_fig(fig_0, fig_1, img_path):
    img_path_0 = img_path.with_name("tmp_0_" + img_path.name)
    img_path_1 = img_path.with_name("tmp_1_" + img_path.name)
    fig_0.savefig(img_path_0, dpi=DPI)
    fig_1.savefig(img_path_1, dpi=DPI)
    im1 = Image.open(img_path_0)
    im2 = Image.open(img_path_1)
    img_path_0.unlink()
    img_path_1.unlink()
    dst = Image.new("RGB", (im1.width + im2.width, im1.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    dst.save(img_path)


################
# Old stuff... #
################


def get_st_size(pdf_document_id):
    import os

    try:
        return os.stat(f"data/fullcorpus/aireg/fulltext/{pdf_document_id}.txt").st_size
    except FileNotFoundError:
        try:
            return os.stat(f"data/fullcorpus/collection/{pdf_document_id}.txt").st_size
        except FileNotFoundError:
            print(f"Missing data: {pdf_document_id}")
            return None


def plot_num_wikitopics(corpus: GraphModels):
    import numpy as np

    corpus.data["_st_size"] = corpus.data.pdf_document_id.map(get_st_size)
    data = corpus.data.copy()
    data["topicnum"] = corpus.data["topics"].map(lambda x: len(x[0]))
    data["size(log)"] = pd.Series(
        pd.cut(corpus.data["_st_size"].map(np.log), 200).map(lambda x: x.mid),
        dtype=float,
    )
    data = data[["size(log)", "topicnum"]]
    plt.clf()
    data.plot("size(log)", "topicnum", kind="hexbin").figure.savefig(
        "hexbin_size(log)_topicnum.pdf"
    )
    plt.clf()
    data.plot("topicnum", kind="hist", bins=200).figure.savefig("hist_size(log).pdf")

    data.groupby(corpus.data["language"])["topicnum"].mean().pipe(lambda x: x[x.gt(0)])
