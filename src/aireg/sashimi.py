import pandas as pd
from sashimi import GraphModels

from . import data
from .data.wikidata_filter import get_filtered_topics


def prepare_data(corpus_data: pd.DataFrame) -> pd.DataFrame:
    """
    Enrich Overton data. Fields we create should start with an underscore.
    """
    corpus_data["_time"] = corpus_data["published_on"].map(
        lambda x: None if x is False else int(x[:4])
    )
    corpus_data["_url"] = corpus_data[["pdf_url", "overton_url"]].agg(list, axis=1)
    # Here we overwrite the topics field with our version
    corpus_data["topics"] = get_filtered_topics(corpus_data["topics"], data.DATA_PATH)
    return corpus_data


def sashimi_bootstrap() -> GraphModels:
    """
    Set up the corpus' parameters and load the data
    """
    corpus_data = data.get_data()
    name = "aireg"
    column = "topics"  # Overton's wikitopics
    columns = {
        "col_title": "title",
        "col_time": "_time",
        "col_url": "_url",
    }
    corpus = GraphModels(column=column, **columns)
    corpus.load_data(prepare_data(corpus_data), name=name)
    corpus.register_config()
    return corpus


def sashimi_reload(config_path="auto_sashimi/reports/config.json") -> GraphModels:
    """
    Reload a corpus already set up
    """
    corpus_data = data.get_data()
    config_path = config_path
    corpus = GraphModels(config_path=config_path, load_data=False)
    name = corpus._to_load["data"].pop()
    corpus.load_data(prepare_data(corpus_data), name=name)
    return corpus


def sample_from_labels(corpus: GraphModels, domain_labels):
    """
    Restrict the corpus to a selection of documents based on a set of domain labels
    """
    corpus.set_sample(None)
    sel = corpus.domain_labels_to_selection(domain_labels())
    corpus.set_sample(sel)
    if not hasattr(corpus, "_orig_analysis_dir"):
        corpus._orig_analysis_dir = corpus.analysis_dir
    corpus.analysis_dir = corpus._orig_analysis_dir / f"sample={domain_labels.__name__}"


def sashimi_chain(corpus: GraphModels, prop, matcher=None):
    """
    Load and register a domain chained model for a given property (column in the data)
    """
    corpus.set_chain(prop=prop, matcher=matcher)
    corpus.load_domain_chained_model()
    corpus.register_config()
