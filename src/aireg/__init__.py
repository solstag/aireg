"""
aireg: a project to study the regulation of "Artificial Intelligence"
"""

# Import graph-tool before pandas to avoid conflicts
import graph_tool  # noqa
import pandas as pd  # noqa

from . import data
from . import exploration
from . import reporting
from . import sashimi

__all__ = ["data", "exploration", "reporting", "sashimi"]

# Switch to holoviews as the backend for pandas plots (requires module `hvplot`)
# pd.options.plotting.backend = "holoviews"
