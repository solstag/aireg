import hvplot.pandas
import bokeh.io

from sashimi import GraphModels
from sashimi.blocks.area_rank_chart import area_rank_chart

from .data import given_labels


def report(corpus: GraphModels, chained=False, split_year=2013):
    """
    Output the usual sashimi interfaces:
    domain map, domain network, area rank chart
    """
    corpus.domain_map(chained=chained)

    dim = "ext" if chained else "ter"
    split_on = corpus.data[corpus.col_time]
    split_list = set(split_on[lambda x: x.ge(split_year)])
    for ylevel in range(1, 4):
        corpus.subxblocks_tables("doc", 3, None, dim, ylevel)
        corpus.domain_network(3, ylevel, split_on=split_on, split_list=split_list)
        corpus.domain_network(2, ylevel, split_on=split_on, split_list=split_list)

    if chained:
        return

    bokeh.io.output_file(corpus.blocks_adir / "area_rank_chart.html")
    bokeh.io.save(
        area_rank_chart(
            corpus,
            corpus.data.index,
            corpus.col_time,
            corpus.col_time,
            3,
            given_labels.AI_domains(),
        )
    )


def make_save_plot(corpus: GraphModels):
    def save_plot(plot, fname, chained=False):
        save_dir = corpus.chained_adir if chained else corpus.blocks_adir
        return hvplot.save(plot, save_dir / fname)

    return save_plot


def histograms(corpus: GraphModels):
    """
    Produce histograms for every column in the data
    """
    df = corpus.data
    for col in df.columns:
        hist = df[col].explode().value_counts()
        hist.to_csv(corpus.data_adir / f"histogram: {col}.csv")
        try:
            plot = hist.hvplot.bar()
            hvplot.save(plot, corpus.data_adir / f"histogram: {col}.html")
        except Exception:
            pass


def source_domain_stat(corpus: GraphModels):
    """
    Statistics about overton policy document sources
    """
    df3 = (
        corpus.data.groupby(["source__source_id", corpus.dblocks[3]])
        .size()
        .unstack()
        .rename(lambda x: corpus.lblock_to_label[3, x], axis=1)
    )
    df3.to_csv(corpus.blocks_adir / "aireg_source__source_id_domains_level_3_all.csv")
    df2 = (
        corpus.data.groupby(["source__source_id", corpus.dblocks[2]])
        .size()
        .unstack()
        .rename(lambda x: corpus.lblock_to_label[2, x], axis=1)
    )
    df2.to_csv(corpus.blocks_adir / "aireg_source__source_id_domains_level_2_all.csv")
