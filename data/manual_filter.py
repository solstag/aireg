import pandas as pd


def get_manual_data(filter=True) -> pd.DataFrame:
    manual_data = pd.read_excel("./data/aireg-domains-classeur1-2023-01-14.ods")
    print(manual_data.columns)
    check_manual_data(manual_data)
    if filter:
        manual_data = manual_data[~manual_data["catégorie"].eq("out of corpus")]
    return manual_data


def check_manual_data(manual_data: pd.DataFrame):
    domain_labels = manual_data["L3D"]
    ld3s = [int(x[x.index("D") + 1 :]) for x in domain_labels]
    for i, n in enumerate(ld3s):
        if i != n:
            raise ValueError(f"index: {i} != label: {n}")
    if manual_data[manual_data["catégorie"].eq("out of corpus")][
        "sous domaine pertinent"
    ].any():
        raise ValueError("out of corpus with pertinent subdomains")
    if not manual_data[~manual_data["catégorie"].eq("out of corpus")][
        "sous domaine pertinent"
    ].all():
        raise ValueError("not out of corpus without pertinent subdomains")


def get_domain_list(manual_data: pd.DataFrame, fine_grained=True) -> list:
    domain_labels = []
    for idx, row in manual_data.iterrows():
        pertinent = row["sous domaine pertinent"]
        if pertinent == "Tous" or not fine_grained:
            domain_labels.append(row["L3D"])
        else:
            domain_labels.extend(label.strip() for label in pertinent.split(";"))
    return domain_labels


def get_topic_lists() -> dict:
    central = ["L1T151", "L1T107", "L1T89", "L1T94"]
    interesting = ["L1T91", "L1T92", "L1T95", "L1T103", "L1T106", "L1T109"]
    in_doubt = ["L1T105", "L1T114", "L1T259", "L1T287", "L1T288"]
    central_and_interesting = central + interesting
    return locals()
