import httpx

from bibliodbs import biblio_overton


def aireg_export_sources_last_added_record(overton_api_key, corpus_data):
    http_client = httpx.Client(timeout=httpx.Timeout(60))
    source_ids = [*corpus_data["source__source_id"].unique()]
    out_path = "aireg_last_updated.xlsx"

    biblio_overton.export_sources_last_added_record(
        api_key=overton_api_key,
        source_ids=source_ids,
        out_path=out_path,
        http_client=http_client,
    )
