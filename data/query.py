"""
Construct a large query from a number of smaller queires based on lists of:
  - AI acronyms, AI terms, REG terms, and ad-hoc terms

Taking the form of:
  - combination of AI and REG near each other
  - AI acronym near REG term, plus an AI term somewhere else
  - ad-hoc term; doesn't require combination

The current query is inspired by an older general query, and a later targeted
query. Those queries are found in './previous_queries.txt'. Notably, we do not
include the term "big data" in AI terms. We note that it is a central term of a
different regulatory issue, and inclusion brings too much noise into the corpus.
Also, its pertinent uses seem covered by other terms.

"""

import pandas as pd

try:
    from bibliodbs.query_downloader import (
        OvertonQueryDownloader,
        get_query_sizes,
        merge_jsonl_datasets,
        download_queries,
    )
except ImportError:
    pass


ai_acronyms = [
    "AI",
]

ai_terms = [
    "artificial intelligence",
    "artificial intelligences",
    "machine intelligence",
    "machine learning",
    "deep learning",
    "algorithm",
    "algorithms",
    "algorithmic decision making",
    "algorithmic decisionmaking",
    "algorithmic management",
    "automated decision making",
    "automated decisionmaking",
    "intelligent systems",
    "intelligent agent",
    "artificial agent",
    "autonomous agent",
    "intelligent agents",
    "artificial agents",
    "autonomous agents",
    "robot",
    "robots",
    "robotics",
    "autonomous robot",
    "autonomous robots",
    "autonomous weapon",
    "autonomous weapons",
    "autonomous vehicle",
    "autonomous vehicles",
    "self-driving vehicle",
    "self-driving vehicles",
    "self-driving car",
    "self-driving cars",
    "facial recognition",
    "face recognition",
    "voice recognition",
    "iris recognition",
    "biometric recognition",
    "biometrical recognition",
    "automated recognition",
    "virtual assistant",
    "virtual assistants",
    "generative adversarial networks",
]

reg_terms = [
    "regulate",
    "regulating",
    "regulation",
    "regulatory",
    "governance",
    "accountability",
    "ethics",
    "ethical",
    "moral",
    "morality",
    "interpretable",
    "interpretability",
    "explainable",
    "explainability",
    "transparent",
    "transparency",
    "fairness",
    "fair",
    "trust",
    "trustworthy",
    "robustness",
    "robust",
    "privacy",
    "liability",
    "responsible",
    "responsibility",
    "politics",
    "policy",
    "policies",
    "takeover",
    "in the workplace",
    "on employment",
    "risk",
    "risks",
    "safety",
    "lethal",
    "ban",
    "existential risk",
    "existential risks",
    "legal personhood",
]

ad_hoc_terms = [
    "superintelligence",
    "artificial general intelligence",
    "intelligence explosion",
    "technological singularity",
    "AGI safety",
    "artificial moral agent",
    "artificial moral agents",
    "autonomy of the moral agent",
    "artificial intelligence control problem",
    "AI control problem",
    "friendly artificial intelligence",
    "friendly AI",
    "deep fake",
    "deepfake",
    "moral machine",
    "moral machines",
    "machine ethics",
    "AI act",
    "artificial intelligence act",
    "AI for good",
    "good AI society",
    "human-machine collaboration",
    "artificial intelligence arms race",
    "artificial intelligence arms control",
    "AI arms control",
    "AI arms race",
    "lethal autonomous weapons",
    "law of robotics",
    "laws of robotics",
]

unsure = [
    "unfair",  # + un...
    "digital labor",
    "digital labour",
    "environmental impact",
    "carbon footprint",
    "copyright",
    "authorship",
    "targeted advertising",
    # + sectors (health, police etc)
]

refused = [
    "GANs",  # only as a last name
]


BASIC_COLS = [
    "overton_url",
    "pdf_url",
    "title",
    "source__title",
    "source__country",
    "source__type",
    "published_on",
    "citation_count",
    "translated_title",
    "authors",
    "topics",
] + [  # columns needed to apply patches from fixes.py
    # "title",
    # "authors",
    "document_url",
    "overton_policy_document_series",
    "source__source_id",
    # "source__title",
    # "policy_source_url",  # not provided by API
    # "source__country",
    "source__region",
]


def gen_term_pairs(terms0, terms1):
    for e0 in terms0:
        for e1 in terms1:
            yield (e0, e1)
            yield (e1, e0)


def gen_query_elements(kind=["combined", "ad_hoc", "acronyms"]):
    """
    Construct the actual queries from the lists of terms
    (kind) which results to include; allows generating only part of the queries
    """
    if "combined" in kind:
        for x, y in gen_term_pairs(ai_terms, reg_terms):
            query_name = query_element = f'"{x} {y}"~2'
            yield query_name, query_element
    if "ad_hoc" in kind:
        for x in ad_hoc_terms:
            query_name = query_element = f'"{x}"'
            yield query_name, query_element
    if "acronyms" in kind:
        for x, y in gen_term_pairs(ai_acronyms, reg_terms):
            query_name = f""""{x} {y}"~2 AND (...)"""
            query_element = (
                f""""{x} {y}"~2 AND ({" OR ".join(f'"{x}"' for x in ai_terms)})"""
            )
            yield query_name, query_element


def get_query_sizes_as_datafarme(api_key, query_sizes=None):
    """
    Get the number of results for each query without downloading
    """
    query_names, query_elements = zip(*gen_query_elements(["combined", "acronyms"]))
    query_parts = [
        (f"element_{n}", s + (None,) * (len(query_elements) - len(s)))
        for n, s in enumerate(zip(*gen_term_pairs(ai_terms + ai_acronyms, reg_terms)))
    ]
    if query_sizes is None:
        query_sizes = get_query_sizes(OvertonQueryDownloader, api_key, query_elements)
    df = pd.DataFrame(
        index=pd.Index(query_names, name="query"),
        data=dict([*query_parts, ("num_records", query_sizes)]),
    )
    return df.sort_index().sort_values("num_records", ascending=False, kind="stable")


def download(api_key, data_dir):
    data_dir.mkdir(exist_ok=False)
    filenames_query = {name: query for name, query in gen_query_elements()}
    download_queries(OvertonQueryDownloader, api_key, filenames_query, data_dir)


def merge_stored_records(dataset_dir, merged_path):
    merge_on_keys = ["pdf_document_id"]
    dataset_paths = [x for x in dataset_dir.iterdir() if x.suffix == ".jsonl"]
    merged_keys_values = merge_jsonl_datasets(merge_on_keys, dataset_paths, merged_path)
    return merged_keys_values
