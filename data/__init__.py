from pathlib import Path
import pyzstd

import pandas as pd

from bibliodbs.biblio_overton import read_records_as_dataframe

from . import query
from . import fixes
from . import checks

__all__ = ["query", "fixes", "checks"]

DATA_PATH = Path("data")
MERGED_PATH = DATA_PATH / "big_query_merged.jsonl.zst"
MERGED_PATCHED_PATH = DATA_PATH / "big_query_merged_patched.json.zst"


def download_and_merge(api_key):
    """
    We download the data for several queries , so we must merge the results
    """
    # Download
    download_dir = DATA_PATH / "query_download"
    if api_key is not None:
        query.download(api_key, download_dir)
    elif not download_dir.is_dir():
        raise ValueError

    # Merge [and compress]
    if MERGED_PATH.exists():
        raise ValueError
    is_compressed = MERGED_PATH.suffix == ".zst"
    merged_path = MERGED_PATH.with_suffix("") if is_compressed else MERGED_PATH
    query.merge_stored_records(download_dir, merged_path)
    if is_compressed:
        with pyzstd.open(MERGED_PATH, "wt") as f:
            with merged_path.open() as f:
                f.write(f.read())
        merged_path.unlink()


def get_new_data(patched=True, columns=True):
    """
    (patched) whether to patch "aireg collection" documents using the original spreadsheets
    (cols) if True, return all columns, if None, returns hardcoded selection
    """
    if patched:
        if not MERGED_PATCHED_PATH.exists() or patched == "redo":
            with pyzstd.open(MERGED_PATH, "rt") as f:
                fixes.patch_and_store(read_records_as_dataframe(f), MERGED_PATCHED_PATH)
        df = pd.read_json(MERGED_PATCHED_PATH, orient="records")
    else:
        with pyzstd.open(MERGED_PATH, "rt") as f:
            df = read_records_as_dataframe(f)
    return (
        df
        if columns is True
        else df[query.BASIC_COLS]
        if columns is None
        else df[columns]
    )


def get_old_data():
    from . import old_data

    return old_data.get_data()


get_data = get_new_data
