"""
Labels given to domains inferred on the '2022-11-11' corpus.
"""


def AI_domains():
    blue = {
        "L3D9": "Police/justice",
        "L3D10": "Discussions diverses",
        "L3D21": "Sécurité du Web",
        "L3D28": "Nécessité de l'encadrement de l'IA",
        "L3D33": "Infrastructures: transport, smart cities",
        "L3D37": "Skills",
        "L3D42": "Armement autononome",
        "L3D50": "Ecole et culture",
        "L3D81": "Surveillance",
    }
    red = {
        "L3D6": "Regards sur fonctionnement de l'IA",
        "L3D7": "Impacts économiques de l'IA",
        "L3D8": "Discussion sur les politiques de l'IA",
        "L3D17": "AGI safety",
        "L3D20": "Débats nationaux sur l'IA",
        "L3D24": "Health care",
        "L3D25": "Prospective sur le travail, la production et les services",
        "L3D26": "IA comme économie créative",
        "L3D39": "IA: investissement, promesse et solution",
    }
    yellow = {
        "L3D0": "Politiques européennes de l'IA",
        "L3D5": "Ethiques et valeurs",
        "L3D22": "Diagnostic santé",
    }
    return blue | red | yellow


def AI_domains_zoom1():
    sel = [
        "L3D9",
        "L3D24",
        "L3D33",
        "L3D42",
        "L3D81",
    ]
    return [x for x in AI_domains() if x in sel]


def AI_domains_zoom2():
    sel = [
        "L3D0",
        "L3D5",
        "L3D6",
        "L3D7",
        "L3D8",
        "L3D17",
        "L3D25",
        "L3D33",
    ]
    return [x for x in AI_domains() if x in sel]
