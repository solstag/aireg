import gzip
import json

import numpy as np
import pandas as pd
from tqdm.auto import tqdm

from arcpath import arcPath

from .. import data
from . import fixes

DATA_PATH = data.DATA_PATH
QUERY_DATA = arcPath(DATA_PATH / "aireg.tar.gz")
CORE_DATA_PATH = DATA_PATH / "aireg20211029.tar.gz"
CORE_DATA = arcPath(CORE_DATA_PATH)
DATA_COMBINED = "combined.json.gz"
DATA_PATCHED = "combined-patched.json.gz"


def get_data():
    data = aireg_data()
    data = data.rename({"overton_document_url": "overton_url"}, axis=1)
    return data


def aireg_data_combined():
    has_combined = (DATA_PATH / DATA_COMBINED).exists()
    if not has_combined:
        data_filter = filter_docs_with_wikitopics_in_ai_and_reg(
            query_inspired_topics_with_unique_grades(
                query_inspired_topics(
                    spreadsheet_path=DATA_PATH / "wikitopics_classification_bilel.xlsx",
                    sheet_name="bilel-plus-with-counts",
                )
            )
        )
        extract_data(
            QUERY_DATA,
            CORE_DATA,
            DATA_PATH / DATA_COMBINED,
            data_filter=data_filter,
        )
    return pd.read_json(DATA_PATH / DATA_COMBINED, orient="records")


def aireg_data():
    has_patched = (DATA_PATH / DATA_PATCHED).exists()
    has_combined = (DATA_PATH / DATA_COMBINED).exists()
    if not has_patched and not has_combined:
        data_filter = filter_docs_with_wikitopics_in_ai_and_reg(
            query_inspired_topics_with_unique_grades(
                query_inspired_topics(
                    spreadsheet_path=DATA_PATH / "wikitopics_classification_bilel.xlsx",
                    sheet_name="bilel-plus-with-counts",
                )
            )
        )
        extract_data(
            QUERY_DATA,
            CORE_DATA,
            DATA_PATH / DATA_COMBINED,
            data_filter=data_filter,
        )
    if not has_patched:
        fixes.patch_and_store(DATA_PATH / DATA_COMBINED, DATA_PATH / DATA_PATCHED)
    return pd.read_json(DATA_PATH / DATA_PATCHED, orient="records")


def get_raw_data(corpus_path):
    # IDEA: restrict which fields to get with a parameter like
    # fields=["pdf_document_id", "title", "topics", "published_on"]
    archive = arcPath(corpus_path)
    files = (x for x in archive.iterarchive() if x.name.endswith(".json"))
    for file in files:
        with file.open() as f:
            for line in f:
                yield json.loads(line)


def merge_data(older_data, newer_data):
    newer_ids = set()
    for x in newer_data:
        newer_ids.add(x["pdf_document_id"])
        yield x
    for x in older_data:
        if x["pdf_document_id"] not in newer_ids:
            yield x


def extract_data(older_path, newer_path, data_path, data_filter=None):
    older_data = get_raw_data(older_path)
    newer_data = get_raw_data(newer_path)
    data = tqdm(merge_data(older_data, newer_data))
    if data_filter:
        data = list(filter(data_filter, data))
    with gzip.open(data_path, "wt") as f:
        json.dump(list(data), f)


def load_data(data_path):
    with gzip.open(data_path, "rt") as f:
        data = json.load(f)
    return data


##############
# FILTERING #
##############


def str_foldfirsts(string):
    return " ".join("".join([x[0].casefold(), x[1:]]) for x in string.split())


def frequency_of_wikitopics(document_data, outfile=None):
    fw = (
        document_data["topics"]
        .explode()
        .map(str)
        .map(str_foldfirsts)
        .value_counts()
        .reset_index()
        .set_axis(["wikipedia_page", "count"], axis=1)
    )
    if outfile:
        fw.to_csv(outfile, index=False)
    return fw


def wikitopics_with_frequencies(
    wikitopics, document_data, outfile="wikitopics_with_counts.csv"
):
    wikitopics["count"] = frequency_of_wikitopics(document_data).set_index(
        "wikipedia_page"
    )["count"]
    wikitopics = wikitopics.sort_values("count", ascending=False)
    if outfile:
        wikitopics.replace({0: np.nan}).to_csv(outfile)
    return wikitopics


def query_inspired_topics(spreadsheet_path, sheet_name, exclude=True):
    query_topics = pd.read_excel(spreadsheet_path, sheet_name=sheet_name)
    query_topics["topic"] = query_topics["topic"].map(str_foldfirsts)
    if exclude and "exclude" in query_topics:
        query_topics = query_topics.loc[~query_topics["exclude"].eq(1)]
        del query_topics["exclude"]
    return query_topics


def query_inspired_topics_with_unique_grades(query_inspired_topics):
    aggregators = {
        "ai": lambda x: x.fillna(0).max(),
        "reg": lambda x: x.fillna(0).max(),
        "count": lambda x: x.fillna(0).sum(),
        "notes": lambda x: "|".join(x.dropna()),
    }
    if "exclude" in query_inspired_topics:
        aggregators["exclude"] = (lambda x: x.fillna(0).max(),)
    assert query_inspired_topics.columns.difference(aggregators) == ["topic"]
    return query_inspired_topics.groupby("topic").agg(aggregators)


def select_docs_with_wikitopics_in_ai_and_reg(wikitopics, data):
    def booleans(container0, container1):
        return (container0.get(str_foldfirsts(str(item)), 0) for item in container1)

    def boolean(booleans):
        return sum(booleans) > 1

    return data["topics"].map(
        lambda x: boolean(booleans(wikitopics["ai"], x))
        and boolean(booleans(wikitopics["reg"], x))
    )


def filter_docs_with_wikitopics_in_ai_and_reg(wikitopics):
    def booleans(container0, container1):
        return (container0.get(str_foldfirsts(str(item)), 0) for item in container1)

    def boolean(booleans):
        return sum(booleans) > 1

    def filter(item):
        return boolean(booleans(wikitopics["ai"], item["topics"])) and boolean(
            booleans(wikitopics["reg"], item["topics"])
        )

    return filter


def compare_selections(document_data, wikitopics, sel_pos, sel_neg, sample=None):
    selected = document_data[sel_pos & ~sel_neg]
    if sample:
        selected = selected.sample(sample)
    for idx in selected.index:
        x = selected.loc[idx]
        print(", ".join([x["title"], x["language"], x["pdf_url"]]))
        y = [str_foldfirsts(z) for z in x["topics"]]
        # print(wikitopics[wikitopics.index.isin(y)])
        print(wikitopics.reindex(y))
        print()


def wikitopics_with_conflicting_grades(query_inspired_topics):
    """Returns duplicated topics with different grades"""
    qt = query_inspired_topics
    # all_dupes = qt.groupby("topic").agg(lambda x: len(x) > 1)
    bad_dupes = qt.groupby("topic").agg(
        lambda x: len(x) > 1
        and not (x.duplicated().sum() == 1 and x.duplicated(keep=False).all())
    )
    bad_dupes = bad_dupes.loc[bad_dupes].index
    bad_topics = qt.loc[qt["topic"].isin(bad_dupes)].sort_values("topic")
    return bad_topics


def fraction_of_docs_with_no_topics(document_data):
    notopics = document_data.loc[~document_data["topics"].map(bool)]
    return len(notopics) / len(document_data)


def store_selection(document_data, sel, outfile="selected.csv"):
    columns = document_data.columns.to_list()
    for col in reversed(["pdf_document_id", "title", "topics"]):
        columns.insert(0, columns.pop(columns.index(col)))
    selected = document_data.loc[sel, columns]
    selected.to_csv(outfile, index=False)
