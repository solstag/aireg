"""
Semantic cleanup of wikitopics

Since wikitopics (topics in Overton) are titles of Wikipedia pages,
we Use the Wikidata ontology to remove those that correspond to
certain types of entities whose presence is undesirable during
domain-topic modeling, such as territories and organizations.
"""

from itertools import chain
import json
import time
from urllib.parse import quote_plus

import httpx
from tqdm import tqdm


def get_filtered_topics(topics, data_path):
    # question = is_territory__or__person_or_organization
    question = is_territory_or_organization
    answers_path = store_answers_for([*chain(*topics)], question, data_path)
    with open(answers_path) as f:
        remove_topic = json.load(f)
    return topics.map(lambda ts: (tuple(t for t in ts if not remove_topic[t]),))


def get_enwiki_resolved_page(title):
    res = httpx.get(
        "https://en.wikipedia.org/w/api.php",
        params={
            "action": "query",
            "titles": title,
            "redirects": "",
            "format": "json",
            "formatversion": "2",
        },
    ).json()
    (page,) = res["query"]["pages"]
    if page.get("missing", False):
        raise ValueError(f"Bad title: {title}")
    return page["title"]


def get_wikidata_query(query):
    res = httpx.get(
        "https://query.wikidata.org/sparql?format=json&query=" + quote_plus(query)
    )
    return res.json()


# Abstract questions


def is_instance_of_subclass(title, classes, log=print):
    title = get_enwiki_resolved_page(title)
    log(f"Querying wikidata for title {title}")
    class_query = " UNION ".join(
        ["{?item wdt:P31/wdt:P279* " + class_ + ".}" for class_ in classes]
    )
    query = (
        '''
    # SELECT DISTINCT ?name ?article ?item WHERE {
    ASK WHERE {
      VALUES ?name { "'''
        + title
        + """"@en } .
      ?article schema:name ?name ;
               schema:about ?item ;
               schema:isPartOf <https://en.wikipedia.org/> ."""
        + class_query
        + """
    }
    """
    )
    return get_wikidata_query(query)["boolean"]


# Concrete questions


def is_territory_or_organization(title, log=print):
    return is_instance_of_subclass(title, ["wd:Q1496967", "wd:Q43229"], log)


def is_territory__or__person_or_organization(title, log=print):
    return is_instance_of_subclass(title, ["wd:Q1496967", "wd:Q106559804"], log)


# Multiple targets


def store_answers_for_at(targets, question, answers_path, answer_if_error=None):
    try:
        with answers_path.open() as f:
            answers = json.load(f)
    except FileNotFoundError:
        answers = {}
    prev_num_answers = len(answers)
    new_targets = {t for t in targets if t not in answers}
    try:
        for target in tqdm(new_targets):
            try:
                answers[target] = question(target, tqdm.write)
            except ValueError:
                answers[target] = answer_if_error
            finally:
                time.sleep(1)
    finally:
        if len(answers) > prev_num_answers:
            with answers_path.open("w") as f:
                json.dump(answers, f)
    if not new_targets:
        return True


def store_answers_for(targets, question, data_path):
    answers_path = data_path / f"wikidata_answers:{question.__name__}.json"
    while True:
        try:
            if store_answers_for_at(targets, question, answers_path):
                break
        except Exception as e:
            print(type(e), e)
            pass
    return answers_path


# Manipulate answers


def remove_answers_with_value(answers_path, value_to_remove, interactive=True):
    with answers_path.open() as f:
        answers = json.load(f)
    new_answers = {k: v for k, v in answers.items() if v != value_to_remove}
    if interactive:
        print(f"Keeping {len(new_answers)} of {len(answers)} answers.")
        breakpoint()
    with answers_path.open("w") as f:
        json.dump(new_answers, f)
