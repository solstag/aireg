import ast
import re
import string

import pandas as pd
import pycountry
from urllib.parse import urlparse


SOURCE_ID = "source__source_id"
DOCUMENT_URL = "document_url"


def get_manual_fixes():
    return pd.read_excel(
        "data/aireg_overton_manual_table_fixed.xlsx", sheet_name="Sheet1"
    )


def get_manual_fix(manual_fixes, field, document_url):
    sel = manual_fixes[DOCUMENT_URL].eq(document_url)
    try:
        (manual_fix,) = manual_fixes.loc[sel, field]
    except ValueError:
        print(f"Missing '{field}' for: {document_url}")
        manual_fix = input("Manual fix:")
    return manual_fix


def patch_in_place(data):
    fixd = fix_airegulation_document_url(data)
    data.loc[fixd.index, fixd.columns] = fixd
    fixd = fix_airegulation_policy_source(data)
    data.loc[fixd.index, fixd.columns] = fixd
    return fixd


def patch_and_store(data_or_path, patched_path):
    if isinstance(data_or_path, pd.DataFrame):
        data = data_or_path
    else:
        data = pd.read_json(data_or_path, orient="records")
    patch_in_place(data)
    data.to_json(patched_path, orient="records")


def gen_manual_table_to_fix(fixd):
    columns_to_inspect = [
        "title",
        "authors",
        DOCUMENT_URL,
        SOURCE_ID,
        "source__title",
        # "source__url",
        "source__country",
        "source__region",
    ]
    fixd.to_excel(
        "aireg_overton_manual_table_to_fix.xlsx",
        columns=columns_to_inspect,
        freeze_panes=(1, 3),
    )


def get_value_and_num_choices(matched, field, choices):
    if len(choices) == 0:
        print("Miss:", field, ">", matched)
        return f"airegulation_miss:{matched}", len(choices)

    if len(choices) == 1:
        (chosen,) = choices
        print("Fixed:", field, ">", chosen)
        return chosen, len(choices)

    if len(choices) > 1:
        print("Multi:", field, ">", choices)
        return f"airegulation_multi:{tuple(choices)}", len(choices)


def fix_with_unique_id(aireg, notaireg, psi_value, idx):
    # Fix source__title
    field = "source__title"
    pst_value, _ = get_value_and_num_choices(
        matched=psi_value,
        field=field,
        choices=(notaireg.loc[notaireg[SOURCE_ID].eq(psi_value), field].unique()),
    )
    aireg.at[idx, field] = pst_value

    # Field source_url not provided by API
    # # Fix policy_source_url
    # field = "policy_source_url"
    # psu_value, _ = get_value_and_num_choices(
    #     matched=psi_value,
    #     fix_field=field,
    #     choices=(
    #         notaireg.loc[notaireg[SOURCE_ID].eq(psi_value), field].unique()
    #     ),
    # )
    # aireg.at[idx, field] = psu_value

    # fix source__country
    field = "source__country"
    psc_value, psc_num = get_value_and_num_choices(
        matched=psi_value,
        field=field,
        choices=(
            notaireg.loc[notaireg[SOURCE_ID].eq(psi_value), field]
            .map(lambda x: x if isinstance(x, str) else tuple(x))  # API X dump
            .unique()
        ),
    )
    aireg.at[idx, field] = (
        (psc_value if isinstance(psc_value, str) else [*psc_value])
        if psc_num == 1
        else psc_value
    )

    # fix policy_source_region
    field = "source__region"
    psr_value, psr_num = get_value_and_num_choices(
        matched=psi_value,
        field=field,
        choices=(
            notaireg.loc[notaireg[SOURCE_ID].eq(psi_value), field].map(tuple).unique()
        ),
    )
    aireg.at[idx, field] = [*psr_value] if psr_num == 1 else psr_value


def fix_no_unique_id(aireg, notaireg, manual_fixes, document_url, idx):
    def country_name_from_tld_suffix(suffix):
        if len(suffix) == 2:
            if suffix == "ai":
                return None
            if suffix == "uk":
                return "UK"
            if suffix == "eu":
                return "EU"
            return pycountry.countries.get(alpha_2=tld_suffix).name
        else:
            if suffix == "gov":
                return "USA"

    field = "source__title"
    (aireg.at[idx, field],) = aireg.loc[idx, "authors"]

    parsed_pdu = urlparse(document_url)

    # Field source_url not provided by API
    # field = "policy_source_url"
    # aireg.at[idx, field] = parsed_pdu._replace(path="", query="", fragment="").geturl()

    field = "source__country"
    psc_is_list = (  # API x dump
        notaireg[field].map(lambda x: isinstance(x, (list, tuple))).any()
    )
    tld_suffix = parsed_pdu.hostname.split(".")[-1]
    if country_name := country_name_from_tld_suffix(tld_suffix):
        aireg.at[idx, field] = [country_name] if psc_is_list else country_name
    else:
        fix_value = ast.literal_eval(get_manual_fix(manual_fixes, field, document_url))
        if psc_is_list:
            aireg.at[idx, field] = fix_value
        else:
            (aireg.at[idx, field],) = fix_value


def clean_url(url):
    parsed = urlparse(url)
    if not (parsed.scheme and parsed.netloc):
        raise ValueError
    return parsed._replace(scheme="", fragment="").geturl()[2:]


def is_airegulation(data):
    return data[SOURCE_ID].eq("airegulation")


def fix_airegulation_policy_source(data):
    """
    Airegulation documents have incorrect policy_source metadata.
    This function checks if document_url partially matches an existing
    document outside airegulation, which thus has good policy_source metadata
    we can copy from.
    """
    columns_to_fix = [
        SOURCE_ID,
        "source__title",
        # "policy_source_url", # not provided by API
        "source__country",
        "source__region",
    ]
    manual_fixes = get_manual_fixes()

    is_aireg = is_airegulation(data)
    aireg = data[is_aireg].copy()
    notaireg = data[~is_aireg]
    aireg_pdu = aireg[DOCUMENT_URL].map(clean_url)
    notaireg_pdu = notaireg[DOCUMENT_URL].map(clean_url)
    num_fixed = {"unique": 0, "no_unique": 0}

    for idx, row in aireg.iterrows():
        print("-" * 20, idx, "-" * 20)

        for col in columns_to_fix:
            aireg.at[idx, col] = None

        document_url = aireg.at[idx, DOCUMENT_URL]
        pdu_start = aireg_pdu.loc[idx]

        print(f"policy_document_url: {document_url}")

        # Find the largest starting substring that overlaps a known url
        while (
            matches := notaireg.loc[notaireg_pdu.str.startswith(pdu_start), SOURCE_ID]
        ).empty and "/" in pdu_start:
            pdu_start = pdu_start[:-1]

        # Fix source_id
        psi_value, psi_num_choices = get_value_and_num_choices(
            matched=pdu_start,
            field=SOURCE_ID,
            choices=matches.unique(),
        )
        if psi_num_choices > 1:
            psi_value = get_manual_fix(manual_fixes, SOURCE_ID, document_url)
            psi_num_choices = 1
        aireg.at[idx, SOURCE_ID] = psi_value

        if psi_num_choices == 1:
            fix_with_unique_id(aireg, notaireg, psi_value, idx)
            num_fixed["unique"] += 1
        elif psi_num_choices == 0:
            fix_no_unique_id(aireg, notaireg, manual_fixes, document_url, idx)
            num_fixed["no_unique"] += 1

    print(num_fixed)

    return aireg


def fix_airegulation_document_url(data):
    """
    airegulation collection has some pdu 'https://manualpdfs.eu-central-1.linodeobjects.com'
    we must figure out the right psu by comparing document features
    with entries in the maual doc table
    """

    def clean_title(title):
        return re.sub(rf"[^{string.ascii_lowercase}0-9]+", "", title.lower())

    def compare_titles(title):
        ct = clean_title(title)
        return ct.startswith(clean_title(row["title"])[: len(ct)])

    # bad_urls = 'https://manualpdfs.eu-central-1.linodeobjects.com'
    url_sources = ["Link_overton", "Link to PDF", "Link"]
    data = data[is_airegulation(data)].copy()

    # Get manual info
    info = pd.read_excel(
        "data/overton_manual_policy_doc_add_rd_13102021_(fix_Link).xlsx",
        sheet_name="Database",
    ).drop_duplicates()
    info["Reference"] = info["Reference"].str.strip()
    info["Issuer"] = info["Issuer"].transform(lambda x: [x])

    for idx, row in data.iterrows():
        print(f"\n{idx}")

        print("Get the ones with good urls first")
        row_choice = row[DOCUMENT_URL]
        matches = info[info[url_sources].eq(row_choice).any(axis=1)]
        if process_matches(matches, row_choice, idx, data, info):
            continue

        print("Now try match by title, authors, overton_policy_document_series")
        sel_title = info["Reference"].map(compare_titles)
        info_choices = info[["Issuer", "Type"]]
        row_choice = row[["authors", "overton_policy_document_series"]].to_list()
        matches = info[sel_title & info_choices.eq(row_choice).all(axis=1)]
        if process_matches(matches, row_choice, idx, data, info):
            continue

        print("Now try match by title, authors")
        sel_title = info["Reference"].map(compare_titles)
        info_choices = info[["Issuer"]]
        row_choice = row[["authors"]].to_list()
        matches = info[sel_title & info_choices.eq(row_choice).all(axis=1)]
        if process_matches(matches, row_choice, idx, data, info):
            continue

        print("Now try match by title")
        matches = info[info["Reference"].map(compare_titles)]
        if process_matches(matches, row_choice, idx, data, info):
            continue

    return data


def process_matches(matches, row_choice, idx, data, info):
    def get_links(matches):
        return tuple(matches["Link"])

    links = get_links(matches)
    if matches.empty:
        print("No matches")
    elif len(matches) > 1:
        print(f"Warning: multiple matches for {row_choice}:")
        print(f"{links}")
        data.loc[idx, DOCUMENT_URL] = f"airegulation_multi:{links}"
    else:
        (data.loc[idx, DOCUMENT_URL],) = links
        info.drop(index=matches.index)
        print("Fixed!")
        return True
